# Nemo point of French stadiums
This Python script computes the Nemo point of French stadiums, i.e. the geographical point which is the furthest from a French stadium.

## Installation
In order to fetch the necessary Python packages, just run `pip3 install -r requirements.txt`.

## Usage
Just run `nemo.py`!

## Output
The output data is generated in the `output` directory, and includes:
* A log with the computed Nemo points.
* Maps representing the Nemo point and the stadiums.

## Acknowledgment
This script uses:
* [GeoJSON of France regions](https://france-geojson.gregoiredavid.fr/), created by Grégoire David.
* A methodology inspired by [Mathieu Garnier](https://web.archive.org/web/20170802062404/http://www.datamix.fr/2016/08/les-plus-gros-trous-perdus-de-france/).
