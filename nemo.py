#!/usr/bin/python3
# encoding=utf-8

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import smopy
import logging
import json
import os
from shapely.geometry import Point, LinearRing
from shapely.geometry.polygon import Polygon
from scipy.spatial import Voronoi, voronoi_plot_2d
from geopy.distance import distance

# Directories definition
input_dir = os.path.dirname(os.path.realpath(__file__)) + '/data'
output_dir = os.path.dirname(os.path.realpath(__file__)) + '/output'
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# Configure logging
logging.basicConfig(filename=output_dir + '/nemo.log', format='%(asctime)-15s %(message)s', level=logging.INFO)

##
# @brief Project a point on a polygon.
#
# @note http://www.itgo.me/a/x5012563757762336606/find-coordinate-of-closest-point-on-polygon-shapely
#
# @param point   Point to project
# @param polygon Polygon on which point should be projected
#
# @return lat, lon Projected point coordinates.
def project_point_on_polygon(point, polygon):
    pol_ext = LinearRing(polygon.exterior.coords)
    d = pol_ext.project(point)
    p = pol_ext.interpolate(d)
    return list(p.coords)[0]

##
# @brief Check if a geographical point is within a GeoJSON description,
#        and project it on the GeoJSON description if this is not the case.
#
# @note https://stackoverflow.com/questions/43892459/check-if-geo-point-is-inside-or-outside-of-polygon
#
# @param geodata GeoJSON description
# @param lat     Point latitude
# @param lon     Point longitude
#
# @return lat, lon Point coordinates within the GeoJSON description.
def project_in_france(geodata, lat, lon):
    # Create geographical point
    point = Point(lon, lat)
    # Projected point, if not already in France
    projected = {
        'lat': 0,
        'lon': 0,
        'distance': 12000
    }

    # Browse all features of the GeoJSON collection to create a list of polygons
    polygons = []
    for f in geodata['features']:
        geom_type = f['geometry']['type']
        # Polygon: only one set of coordinates
        if geom_type == 'Polygon':
            polygons.append({
                'p': Polygon(f['geometry']['coordinates'][0]),
                'name': f['properties']['nom']
            })
        # MultiPolygon: several sets of coordinates
        elif geom_type == 'MultiPolygon':
            for p in f['geometry']['coordinates']:
                polygons.append({
                    'p': Polygon(p[0]),
                    'name': f['properties']['nom']
                })
        else:
            raise Exception('Unsupported type: {}'.format(geom_type))

    # Browse all polygons
    for polygon in polygons:
        if polygon['p'].contains(point):
            # Point is within the polygon, just return
            logging.debug('Point {}, {} located in {}'.format(lat, lon, polygon['name']))
            return lat, lon
        else:
            # Point is not within the polygon, project it and find minimal distance
            closest_p = project_point_on_polygon(point, polygon['p'])
            dist = distance((closest_p[1], closest_p[0]), (lat, lon)).km
            if dist < projected['distance']:
                projected['lat'] = closest_p[1]
                projected['lon'] = closest_p[0]
                projected['distance'] = dist

    logging.debug('Point {}, {} projected to {}, {} ({} km)'.format(lat, lon, projected['lat'], projected['lon'], round(projected['distance'], 2)))
    return projected['lat'], projected['lon']

# Read the CSV file containing the raw data
stadiums = pd.read_csv(input_dir + '/stades.csv', sep=';', header=0)

# Retrieve tiles to create the maps
logging.info('Retrieving tiles')
france_map = smopy.Map((43., -1., 51., 8.), z=6)
logging.info('Tiles retrieved')

# Read the GeoJSON representation of France
for geojson_file in ['regions_Metropole.geojson', 'regions_Metropole_sans_Corse.geojson']:
    geojson = open(input_dir + '/' + geojson_file, 'r')
    geodata = json.load(geojson)
    start = geojson_file.find('regions_') + 8
    end = geojson_file.find('.geojson', start)
    file_suffix = geojson_file[start:end]

    # Browse all leagues
    for league in ['Ligue 1', 'Ligue 2', 'Ligue 1 Ligue 2']:
        # Browse all seasons
        for season in stadiums.columns[4:].values:
            season_str = '{}-{}'.format(season, int(season) - 1999)
            logging.info('Computing Nemo point for {}, {}, season {}'.format(file_suffix.replace('_', ' '), league, season_str))

            # Initialization
            new_map = france_map
            map_points = []
            if not league == 'Ligue 1 Ligue 2':
                lat = stadiums[(stadiums[season] == league)]['Latitude'].values
                lon = stadiums[(stadiums[season] == league)]['Longitude'].values
                name = stadiums[(stadiums[season] == league)]['Stade'].values
            else:
                lat = stadiums[(stadiums[season].notna())]['Latitude'].values
                lon = stadiums[(stadiums[season].notna())]['Longitude'].values
                name = stadiums[(stadiums[season].notna())]['Stade'].values

            # 1. Compute Voronoi diagram: vertices are furthest points from stadiums.
            vor = Voronoi(np.c_[lat, lon])
            # Project vertices to France if necessary
            vertices = [project_in_france(geodata, v[0], v[1])
                        for v in vor.vertices
                        if -90 <= v[0] <= 90 and -90 <= v[1] <= 90]

            # 2. Compute distance from each vertice to all stadiums, and keep the minimum.
            nemos = []
            for vertice in vertices:
                # Compute all distances
                distances = []
                for i in range(0, len(lat)):
                    x, y = new_map.to_pixels(lat[i], lon[i])
                    map_points.append({'type': 'stadium', 'x': x, 'y': y})
                    distances.append({
                        'lat': lat[i],
                        'lon': lon[i],
                        'stadium': name[i],
                        'd': distance(vertice, (lat[i], lon[i])).km
                    })
                # Find the minimal distance
                distances = sorted(distances, key=lambda x: x['d'])
                nemos.append({
                    'lat': vertice[0],
                    'lon': vertice[1],
                    'distances': [{'km': d['d'], 'stadium': d['stadium']} for d in distances]
                })

            # 3. From all these possible nemo points, keep the one furthest from a stadium
            nemo = max(nemos, key=lambda x: x['distances'][0]['km'])
            x, y = new_map.to_pixels(nemo['lat'], nemo['lon'])
            map_points.append({'type': 'nemo', 'x': x, 'y': y})
            logging.info('Nemo point: {}, {}'.format(round(nemo['lat'], 6), round(nemo['lon'], 6)))
            logging.info('Stadium distances:')
            for d in nemo['distances']:
                logging.info('\t{:30s}: {:.2f} km'.format(d['stadium'], round(d['km'], 2)))

            # 4. Add all points to map
            ax = new_map.show_mpl(figsize=(4, 4), dpi=200)
            for point in map_points:
                marker = '.b' if point['type'] == 'nemo' else '.r'
                ax.plot(point['x'], point['y'], marker, ms=5)
            ax.text(0, 0, '{}: {}, {}'.format(file_suffix.replace('_', ' '), league, season_str), fontsize=12)
            # Save map to file
            plt.savefig('{}/Nemo_{}_{}_{}.png'.format(output_dir, file_suffix, league.replace(' ', '_'), season))
            plt.close()
